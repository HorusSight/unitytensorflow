﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using TensorFlowLite;

public class DeepLabSample : MonoBehaviour
{
    [SerializeField, FilePopup("*.tflite")] string fileName = "deeplabv3_257_mv_gpu.tflite";
    [SerializeField] RawImage cameraView = null;
    [SerializeField] RawImage outputView = null;
    [SerializeField] ComputeShader compute = null;

    WebCamTexture webcamTexture;
    DeepLab deepLab;

    arxHeadset arx;

    void Start()
    {
        arx = this.GetComponent<arxHeadset>();
        string path = Path.Combine(Application.streamingAssetsPath, fileName);
        deepLab = new DeepLab(path, compute);

        // Init camera
        //string cameraName;
        ////webcamTexture = new WebCamTexture(cameraName, 640, 480, 30);
        ////webcamTexture.Play();




        //WebCamDevice[] devices = WebCamTexture.devices;

        //for (int i = 0; i < devices.Length; i++)
        //{
        //    //Debug.Log(devices[i].name);

        //    string deviceName = devices[i].name;

        //    if (deviceName.Contains("StereoCam"))
        //    {
        //        //|| devices[i].name == "StereoCam #2") {
        //        cameraName= devices[i].name;
        //        webcamTexture = new WebCamTexture(cameraName, 1280, 480);
        //        Debug.Log("Arx Headset found");
        //        webcamTexture.deviceName = devices[i].name;
        //        webcamTexture.Play();
        //        //Debug.Log("Resolution:" + arxResolution.x + ", " + arxResolution.y);

        //    }
        //}

        //cameraView.texture = arx.leftImage.texture;


    }

    void OnDestroy()
    {
        webcamTexture?.Stop();
        deepLab?.Dispose();
    }

    void Update()
    {


        

        //deepLab.Invoke(arx.returnStereoInputFrame(webcamTexture));
        deepLab.Invoke(arx.leftImage.texture);

        cameraView.texture = arx.leftImage.texture;
        // Slow works on mobile
        outputView.texture = deepLab.GetResultTexture2D();

        // Fast but errors on mobile. Need to be fixed 
        // outputView.texture = deepLab.GetResultTexture();

        cameraView.material = deepLab.transformMat;
    }

}
