﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using OpenCVForUnity.CoreModule;
using OpenCVForUnity.ImgprocModule;
using OpenCVForUnity.UtilsModule;
using OpenCVForUnity.UnityUtils;
using OpenCVForUnity.Calib3dModule;
//using OpenCVForUnity.Core;

public class arxHeadset : MonoBehaviour
{

    //public RawImage cameraDisplay;
    WebCamTexture webcamTexture;



    public RawImage leftImage;
    protected Mat baseMat;
    protected Color32[] colors;

    
    Mat leftMat;
   




    void Start()
    {


        webcamTexture = new WebCamTexture("stereo", 1280, 480, 30);
        //cameraDisplay.texture = webcamTexture;
        Debug.Log("searching for cameras");


        listCameraDevice();
    }

    private void Update()
    {


        leftImage.texture = returnStereoInputFrame(webcamTexture);
    }


    void listCameraDevice()
    {

        WebCamDevice[] devices = WebCamTexture.devices;


        for (int i = 0; i < devices.Length; i++)
        {
            Debug.Log(devices[i].name);

            string deviceName = devices[i].name;

            if (deviceName.Contains("StereoCam"))
            {
                //|| devices[i].name == "StereoCam #2") {

                Debug.Log("Arx Headset found");
                webcamTexture.deviceName = devices[i].name;
                webcamTexture.Play();
                //Debug.Log("Resolution:" + arxResolution.x + ", "+arxResolution.y);
              

            }
        }
    }


    public Texture2D returnStereoInputFrame(WebCamTexture webcamTextureIn) {

        colors = new Color32[webcamTextureIn.width * webcamTextureIn.height];
        baseMat = new Mat(webcamTextureIn.height, webcamTextureIn.width, CvType.CV_8UC4);
        //baseMat = new Mat(webcamTextureIn.width, webcamTextureIn.height, CvType.CV_8UC4);


        Utils.webCamTextureToMat(webcamTextureIn, baseMat, colors, false);
        Texture2D mainTexture = new Texture2D(baseMat.cols(), baseMat.rows(), TextureFormat.RGBA32, false);

        Utils.matToTexture2D(baseMat, mainTexture);
        //cameraDisplay.texture = mainTexture;

        //cropping

        OpenCVForUnity.CoreModule.Rect rectCrop = new OpenCVForUnity.CoreModule.Rect(0, 0, webcamTextureIn.width / 2, webcamTextureIn.height);
        leftMat = new Mat(baseMat, rectCrop);
        //Texture2D leftTexture = new Texture2D(leftMat.rows(), leftMat.cols(), TextureFormat.RGBA32, false);
        Texture2D leftTexture = new Texture2D(leftMat.cols(), leftMat.rows(), TextureFormat.RGBA32, false);

        //Core.transpose(leftMat, leftMat);
        //Core.flip(leftMat, leftMat, 0);
        Utils.matToTexture2D(leftMat, leftTexture);

    
        //textureL = leftTexture;

        return leftTexture;
        //return mainTexture;


    }

    //void displayStereoInput()
    //{



    //    colors = new Color32[webcamTexture.width * webcamTexture.height];
    //    baseMat = new Mat(webcamTexture.height, webcamTexture.width, CvType.CV_8UC4);

    //    Utils.webCamTextureToMat(webcamTexture, baseMat, colors, false);
    //    Texture2D mainTexture = new Texture2D(baseMat.cols(), baseMat.rows(), TextureFormat.RGBA32, false);

    //    Utils.matToTexture2D(baseMat, mainTexture);
    //    //cameraDisplay.texture = mainTexture;

    //    //cropping

    //    OpenCVForUnity.CoreModule.Rect rectCrop = new OpenCVForUnity.CoreModule.Rect(0, 0, webcamTexture.width / 2, webcamTexture.height);
    //    leftMat = new Mat(baseMat, rectCrop);
    //    Texture2D leftTexture = new Texture2D(leftMat.rows(), leftMat.cols(), TextureFormat.RGBA32, false);


    //    Core.transpose(leftMat, leftMat);
    //    Core.flip(leftMat, leftMat, 0);
    //    Utils.matToTexture2D(leftMat, leftTexture);

    //    leftImage.texture = leftTexture;
    //    textureL = leftTexture;

    //    OpenCVForUnity.CoreModule.Rect rectCropR = new OpenCVForUnity.CoreModule.Rect(webcamTexture.width / 2, 0, webcamTexture.width / 2, webcamTexture.height);
    //    rightMat = new Mat(baseMat, rectCropR);
    //    Texture2D rightTexture = new Texture2D(rightMat.rows(), rightMat.cols(), TextureFormat.RGBA32, false);
    //    Core.transpose(rightMat, rightMat);
    //    Core.flip(rightMat, rightMat, 0);
    //    Utils.matToTexture2D(rightMat, rightTexture);
    //    rightImage.texture = rightTexture;




    //}


    //Texture2D computeDepthMap()
    //{

    //    //# Get optimal camera matrix for better undistortion 
    //    //        new_camera_matrix, roi = cv2.getOptimalNewCameraMatrix(K, dist, (w, h), 1, (w, h))
    //    //#Undistort images
    //    //img_1_undistorted = cv2.undistort(img_1, K, dist, None, new_camera_matrix)
    //    //img_2_undistorted = cv2.undistort(img_2, K, dist, None, new_camera_matrix)






    //    Mat imgDisparity16S = new Mat(leftMat.rows(), leftMat.cols(), CvType.CV_16S);
    //    Mat imgDisparity8U = new Mat(rightMat.rows(), rightMat.cols(), CvType.CV_8UC1);

    //    //if (imgLeft.empty() || imgRight.empty())
    //    //{
    //    //    Debug.Log("Error reading images ");
    //    //}

    //    P1 = 3 * win_size;
    //    //P1 *= P1;
    //    P2 = 3 * win_size;
    //    //P2 *= P2;
    //    //StereoBM sbm = StereoBM.create(disparity, blocksize);
    //    //sbm.compute(rightMat, leftMat, imgDisparity16S);

    //    StereoSGBM SGBM = StereoSGBM.create(min_disp, disparity, blocksize, P1, P2, disp12MaxDiff, perFilterCap, UniquenessRatio, speckleWindowSize, speckleRange, 1);
    //    SGBM.compute(leftMat, rightMat, imgDisparity16S);

    //    //normalize to CvType.CV_8U
    //    Core.normalize(imgDisparity16S, imgDisparity8U, 0, 255, Core.NORM_MINMAX, CvType.CV_8U);

    //    Texture2D texture = new Texture2D(imgDisparity8U.cols(), imgDisparity8U.rows(), TextureFormat.RGBA32, false);

    //    Utils.matToTexture2D(imgDisparity8U, texture);

    //    return texture;
    //}
}
