﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playWithDelay : MonoBehaviour
{

    AudioSource asource;
    float delay = 0.05f;
    bool init = false;
    bool readyToPlay = true;



    public void attachAudioSource(AudioSource asrc) {
        asource = asrc;
      
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }



    // Update is called once per frame
    void FixedUpdate()
    {
        

        if (!asource.isPlaying && init && readyToPlay) {

            readyToPlay = false;
            StartCoroutine("WaitAndPlay");
        }
        
    }

    public void playLoopedDelay(float newDelay) {

        delay = newDelay;
        init = true; // then update will play

    }

    IEnumerator WaitAndPlay()
    {
        
        // suspend execution for X seconds
        yield return new WaitForSeconds(delay);
        asource.Play();
        readyToPlay = true;
        Debug.Log("Playing");

    }
}
