﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using TensorFlowLite;

public class SsdSample : MonoBehaviour
{
    [SerializeField, FilePopup("*.tflite")] string fileName = "coco_ssd_mobilenet_quant.tflite";
    [SerializeField] RawImage cameraView = null;
    [SerializeField] Text framePrefab = null;
    [SerializeField, Range(0f, 1f)] float scoreThreshold = 0.5f;
    [SerializeField] TextAsset labelMap = null;

    //aim
    Vector2 center = new Vector2(0, 0);
    public Image centerCircle;
    public Image objCircle;
    Vector3 cubeOffset;
    float estimatedDepth;
    public float Xmultipler = 10.0f;
    public AudioSource audioSourceDD;
        public Canvas myCanvas;
    float distance = 0.0f;

    WebCamTexture webcamTexture;
    SSD ssd;

    Text[] frames;

    public string[] labels;


    
    arxHeadset arx;
    GameObject[] boxesResult = new GameObject[10];
    public GameObject cubeInstance;

    void Start()
    {
        arx = this.GetComponent<arxHeadset>();
        string path = Path.Combine(Application.streamingAssetsPath, fileName);
        ssd = new SSD(path);

        // Init camera
        //string cameraName = WebCamUtil.FindName();
        //webcamTexture = new WebCamTexture(cameraName, 1280, 720, 30);
        //cameraView.texture = webcamTexture;
        //webcamTexture.Play();
        //Debug.Log($"Starting camera: {cameraName}");

        // Init frames
        frames = new Text[10];
        var parent = cameraView.transform;

        //center.x = cameraView.rectTransform.rect.width / 2;
        //center.y = cameraView.rectTransform.rect.height / 2;
        center.x = myCanvas.GetComponent<RectTransform>().rect.width/2;
        center.y = myCanvas.GetComponent<RectTransform>().rect.height / 2;
        centerCircle.transform.position = center;
        cubeOffset = cubeInstance.transform.position;


        //Debug.Log("center is: " + center);

        for (int i = 0; i < frames.Length; i++)
        {
            frames[i] = Instantiate(framePrefab, Vector3.zero, Quaternion.identity, parent);
        }

        // Labels
        labels = labelMap.text.Split('\n');


    }

    void OnDestroy()
    {
        webcamTexture?.Stop();
        ssd?.Dispose();
    }

    void Update()
    {
        emptyBoxArray();
        ssd.Invoke(arx.leftImage.texture);

        cameraView.texture = arx.leftImage.texture;
        var results = ssd.GetResults();

        var size = cameraView.rectTransform.rect.size;

        for (int i = 0; i < 1; i++)
        {
            SetFrame(frames[i], results[i], size);
            //setBoxes(frames[i], results[i], size, i);

            //Debug.Log(results[0].rect.position * size - size * 0.5f);
            Vector2 abstractedPosition = (results[0].rect.position * size - size * 0.5f) ;
            float offset = (results[0].rect.width * size.x) / 2;
            float x = abstractedPosition.x + center.x + offset;
            objCircle.transform.position = new Vector2(abstractedPosition.x + center.x + offset , center.y);


            distance = Vector2.Distance(center, objCircle.transform.position);

            Debug.Log("estimated 2D distance from center = " + distance);
            //compare size of frame versus canvas
            float widthRatio =  cameraView.rectTransform.rect.width / results[0].rect.width;
            float lengthRatio =  cameraView.rectTransform.rect.height / results[0].rect.height;
            estimatedDepth = (widthRatio + lengthRatio) / 2; //as an average
            estimatedDepth /= 100;

            //should probably do a remap?

            //Debug.Log("estimated depth = " + estimatedDepth);
            //if ratio is 1 distance is low, if ratio is closer to 0, distance is high and therefor correct

            Vector3 directionalOffset = Camera.main.ScreenToWorldPoint(new Vector3(objCircle.transform.position.x, objCircle.transform.position.y, Camera.main.nearClipPlane));
            Vector3 targetPosition = new Vector3(cubeOffset.x + directionalOffset.x * Xmultipler, cubeOffset.z + directionalOffset.z, cubeOffset.y + directionalOffset.y + estimatedDepth);


            Vector3 velocity = Vector3.zero;
            cubeInstance.transform.position = Vector3.SmoothDamp(cubeInstance.transform.position, targetPosition, ref velocity, 0.2f) ;
            if (audioSourceDD){

                //-1.0 = Full left 0.0 = center 1.0 = full right

                //remap the value
                float stereoValue = objCircle.transform.position.x;
                float halfWidth = cameraView.rectTransform.rect.width / 2;
                stereoValue = remap(stereoValue, -halfWidth, halfWidth, -1, 1);

                //Debug.Log("STEREO is: " + stereoValue+ "half width = " + halfWidth);

                audioSourceDD.panStereo = stereoValue;
                //

            }
        }


       
        //find the center of the canvas


        //move cube on X axis






        cameraView.material = ssd.transformMat;
        // cameraView.texture = ssd.inputTex;
    }

    float remap(float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }


    void emptyBoxArray()
    {
        for (int i = 0; i < boxesResult.Length; i++)
        {
            Destroy(boxesResult[i]);
        }
    }

    void setBoxes(Text frame, SSD.Result result, Vector2 size, int index) {

        //add to box array and then delete again

        //GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        GameObject cube = Instantiate(cubeInstance);
        Vector2 position = result.rect.position * size - size * 0.5f;
        cube.transform.position = new Vector3(position.x / 10, position.y / 10, 0.5f);

        boxesResult[index] = cube;

    }



    void SetFrame(Text frame, SSD.Result result, Vector2 size)
    {
        if (result.score < scoreThreshold)
        {
            frame.gameObject.SetActive(false);
            return;
        }
        else
        {
            frame.gameObject.SetActive(true);
        }

        frame.text = $"{GetLabelName(result.classID)} : {(int)(result.score * 100)}%";
        var rt = frame.transform as RectTransform;
        rt.anchoredPosition = result.rect.position * size - size * 0.5f;
        rt.sizeDelta = result.rect.size * size;
    }

    string GetLabelName(int id)
    {
        if (id < 0 || id >= labels.Length - 1)
        {
            return "?";
        }
        return labels[id + 1];
    }

}
