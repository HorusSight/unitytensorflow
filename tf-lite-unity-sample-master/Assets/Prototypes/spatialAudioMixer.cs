﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class spatialAudioMixer : MonoBehaviour
{
    public enum demoOptionType { pitch, delay}
    public demoOptionType demoType = demoOptionType.pitch;

    public RawImage actor;
    Vector2 center;
    float distance;
    public Canvas myCanvas;

     float range01 = 121.0f;
     float range02 = 206.0f;
     float range03 = 326.0f;
     float range04 = 371.0f;

    AudioSource audioSource;
    float halfWidth;
    enum ranges { range01, range02, range03, range04};
    ranges range;

    playWithDelay delayPlayer;

    // Start is called before the first frame update
    void Start()
    {
        center.x = myCanvas.GetComponent<RectTransform>().rect.width / 2;
        center.y = myCanvas.GetComponent<RectTransform>().rect.height / 2;

        range01 = myCanvas.GetComponent<RectTransform>().rect.width / 5;
        range02 = myCanvas.GetComponent<RectTransform>().rect.width / 3;
        //draw circles

        audioSource = this.GetComponent<AudioSource>();
        delayPlayer = this.GetComponent<playWithDelay>();
        delayPlayer.attachAudioSource(audioSource);

        var rect = myCanvas.transform as RectTransform;
        halfWidth = rect.rect.width / 2;
    }

    // Update is called once per frame
    void Update()
    {
        distance = Mathf.Abs(Vector2.Distance(actor.transform.position, center));
       
        //define ranges // assign behaviours
        if (distance > range02) {
            
            range = ranges.range03;
        
            switch (demoType) {
                case demoOptionType.delay:
                    delayPlayer.playLoopedDelay(2.5f);
                    break;

                case demoOptionType.pitch:
                    audioSource.pitch = 1.0f;
                    break;


            }


        }
        else if (distance > range01 && distance <= range02) {
            
            range = ranges.range02;

            switch (demoType)
            {
                case demoOptionType.delay:
                    delayPlayer.playLoopedDelay(1.0f);
                    break;

                case demoOptionType.pitch:
                    audioSource.pitch = 2.0f;
                    break;


            }

        }
        else if(distance <= range01){
            
            range = ranges.range01;

            switch (demoType)
            {
                case demoOptionType.delay:
                    delayPlayer.playLoopedDelay(0.0f);
                    break;

                case demoOptionType.pitch:
                    audioSource.pitch = 3.0f;
                    break;


            }
        }




        Debug.Log(range + ": " + distance);
        //mix stereo Audio Channels
        //float test = myCanvas.GetComponentInParent<Rect>().width;

        float stereoValue = actor.transform.position.x;
        stereoValue = remap(stereoValue, 0, halfWidth*2, -1, 1);
        //Debug.Log(actor.transform.position.x + " , " + stereoValue + " , " + halfWidth);
        audioSource.panStereo = stereoValue*1.8f;

    }


    float remap(float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }
}
